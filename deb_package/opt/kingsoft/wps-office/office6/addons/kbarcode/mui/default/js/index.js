;(function(global, $){
	var DropDown = function($elem, options)
	{
		var that = this;
		this.$elem = $elem;
		this.$dropdownWidget = $('<div/>');
		this.options = options;
		this.value = '';
		this.index = 0;
		this.$items = [];

		this.$dropdownWidget
			.css('display', 'none')
			.css('position', 'absolute')
			.css('z-index', 1)
			.width(this.options.dropdownWidgetSize[0])
			.height(this.options.dropdownWidgetSize[1])
			.addClass(this.options.dropdownWidgetClass)
			.appendTo(this.$elem)
			.click(function(e){
				e.stopPropagation();
			})
			;

		this.$text = $('<span/>').addClass(this.options.spanClass);

		this.select = function(idx)
		{
			var item = this.options.listArray[idx];
			this.$text.html(item.text);
			this.value = item.value;
			this.index = idx;
		}

		var $dropdownWidgetItemContainer = $('<div/>')
			.css('display', 'inline-block')
			.addClass(this.options.dropdownListClass)
			.width(this.options.dropdownListSize[0])
			.height(this.options.dropdownListSize[1])
			.appendTo(this.$dropdownWidget);

		this.$dropdownBillboard = $('<div/>')
			.css('display', 'inline-block')
			.css('position', 'relative')
			.width(this.options.dropdownWidgetSize[0] - this.options.dropdownListSize[0] - 2/*border*/)
			.height(this.options.dropdownListSize[1])
			.addClass(this.options.dropdownBillboard)
			.appendTo(this.$dropdownWidget);

		for (var i = 0; i < this.options.listArray.length; i++)
		{
			var clickCallback = function(idx){
				return function(e){
					that.$dropdownWidget.hide();
					that.$elem.removeClass('active');

					if (that.index !== idx)
					{
						that.select(idx);
						if (that.options.changed)
							that.options.changed.call(that);
					}

					that.options.hidden && that.options.hidden.call(that);
					e.stopPropagation();
				}
			}

			var hoverCallback = function(idx){
				return function(){
					that.options.hover.call(that, that.options.listArray[idx]);
				}
			}

			this.$items[i] = $('<span/>').html(this.options.listArray[i].text)
						.appendTo(
							$('<div/>')
								.addClass(this.options.dropdownItemClass)
								.appendTo($dropdownWidgetItemContainer)
								.click(clickCallback(i))
								.hover(hoverCallback(i))
						);
		}

		var invSelect = function()
		{
			that.$dropdownWidget.hide();
			that.$elem.removeClass('active');
			that.options.hidden && that.options.hidden.call(that);
		}

		var listArray = this.options.listArray;
		this.$elem
			.css('display', 'inline-block')
			.addClass(this.options.dropdownClass)
			.append(this.$text)
			.click(function(e){
					for (var i = 0; i < that.options.listArray.length; i++)
					{
						that.$items[i].parent().removeClass('checked');
					}
					that.$items[that.index].parent().addClass('checked');

					if (!that.$elem.hasClass('active'))
					{
						that.$dropdownWidget
							.css('left', that.$elem.offset().left)
							.css('top', that.$elem.offset().top + that.$elem.height() + 3)
							.show();
						that.$elem.addClass('active');
						that.options.clicked && that.options.clicked.call(that);
						that.options.showed && that.options.showed.call(that);
					}
					else
					{
						invSelect();
					}

					e.stopPropagation();
				});

		if (this.options.dropdownImage !== undefined)
			this.$elem.css('background', 'url(' + this.options.dropdownImage + ') no-repeat right center');

		this.select(0);

		$('html').click(function(){
			invSelect();
		});

		this.methods = {
			value: function(arg)
			{
				if (arg === undefined)
				{
					return that.value;
				}
				else
				{
					for (var i = 0; i < that.options.listArray.length; i++)
					{
						if (arg === that.options.listArray[i].value)
						{
							that.select(i);
							break;
						}
					}
					return that.$elem;
				}
			},
			select: function(idx)
			{
				that.$items[idx].click();
			},
			indexOf: function(value)
			{
				for (var i = 0; i < that.options.listArray.length; i++)
				{
					var item = that.options.listArray[i];
					if (item.value === value)
						return i;
				}
				return -1;
			}
		};
	}

	var barcodeDropdown = function()
	{
		$.fn.extend({
			dropdown: function(arg1, arg2)
			{
				var $this = $(this);
				if (typeof(arg1) === 'object' && arg2 === undefined)
				{
					// 新建的场景
					$(this).data('kbarcode-dropdown', new DropDown($(this), arg1));
					return $(this);
				}
				else
				{
					return $this.data('kbarcode-dropdown').methods[arg1](arg2);
				}
			}
		})
	}

	var maxLength = {
		EAN: 13,
		UPC: 12,
		pharmacode: 5,
		ITF14: 14,
		DEFAULT: 64,
	}

	var getLength = function(fmt)
	{
		if (!maxLength[fmt])
			return maxLength['DEFAULT'];
		return maxLength[fmt];
	}

	var adjust = function()
	{
		var format = $('#dropdown').dropdown('value');
		var content = $('#content').val();
		var len = getLength(format);

		if (content.length > len)
		{
			$('#content').val(content.substr(0, len));
			adjust();
			return true;
		}

		$('#cnt').html(
			content.length + '/' + len
			);
		return false;
	}

	var getFormat = function(format, content)
	{
		if (format === 'EAN')
		{
			switch (content.length)
			{
				case 13:
				case 12:
					format = 'EAN13';
					break;
				case 8:
				case 7:
					format = 'EAN8';
					break;
				case 5:
					format = 'EAN5';
					break;
				case 2:
					format = 'EAN2';
					break;
			}
		}
		return format;
	}

	var updateUI = function(validInput)
	{
		if (!validInput)
		{
			$('#insert').addClass('disabled');
		}
		else
		{
			if ($('#insert').hasClass('disabled'))
				invoker.invoke("collect", {id: 4});
			$('#insert').removeClass('disabled');
		}
	}

	var clearPreview = function()
	{
		$('#result').attr('src', '');
	}

	var sizeWatcher = function(timeout)
	{
		var $result = $('#result'),
			$preview = $result.parent('.preview');
			
		if ($result.width() > $preview.width())
		{
			var scale = $preview.width() / $result.width();
			$result
				.width($preview.width())
				.height($result.height() * scale)
				.css('left', '0px')
				.css('top', ($preview.height() - $result.height() - 20) / 2 - 10 + 'px');
		}

		$result.show();

		setTimeout(sizeWatcher, timeout);
	}

	var viewBarcode = function()
	{
		var needAdjust = adjust();
		var format = $('#dropdown').dropdown('value');
		var category_format = format;
		var content = $('#content').val();
		if (content.length === 0)
		{
			updateUI(false);
			return;
		}

		format = getFormat(format, content);
		try
		{
			var showException = true;
			var hasTargetLength = false;

			var len = content.length;
			if (category_format === 'EAN')
			{
				var target_format, target_length;
				hasTargetLength = true;

				if (len <= 2)
				{
					target_length = 2;
					target_format = 'EAN2';
				}
				else if (len <= 5)
				{
					target_length = 5;
					target_format = 'EAN5';
				}
				else if (len <= 7)
				{
					target_length = 7;
					target_format = 'EAN8';
				}
				else if (len <= 12)
				{
					target_length = 12;
					target_format = 'EAN13';
				}
			}
			else if (category_format === 'UPC')
			{
				hasTargetLength = true;
				target_length = 12;
				target_format = 'UPC';
			}
			else if (category_format === 'ITF14')
			{
				hasTargetLength = true;
				target_length = 14;
				target_format = 'ITF14';
			}

			if (hasTargetLength && len < target_length)
			{
				$('#log')
					.removeClass()
					.addClass('log-info')
					.html(
						'继续输入' + (target_length - len) + '个数字，将使用' + target_format + '编码生成条形码'
						)
				showException = false;
			}
			
			var $result = $('#result'),
				$preview = $result.parent('.preview');

			clearDemo();

			JsBarcode("#result", content, {
				format: format,
				displayValue: true,
				fontSize:14,
				lineColor: "#000",
				height: 80
			});

			if (needAdjust)
				$('#log')
				.removeClass()
				.addClass('log-info')
				.html('输入已达最大值');
			else
				$('#log').html('　');

			updateUI(true);
		}
		catch (msg)
		{
			if (showException)
			{
				$('#log')
					.removeClass()
					.addClass('log-error')
					.html('内容中包含不支持的字符或输入有误');

				invoker.invoke("collect", {id: 5});
				clearPreview();
			}

			updateUI(false);
		}
	}

	var loadData = function()
	{
		// 向客户端请求数据
		invoker.invoke("loadData", {}, function(data){
			if (!data.create)
			{
				var idx = $('#dropdown').dropdown('indexOf', data.format);
				$('#dropdown').dropdown('select', idx);
				$('#content').val(data.text);
				viewBarcode();
			}
			else
			{
				var value = $('#dropdown').dropdown('value');
				viewDemo(value);
				$('#content').attr('placeholder', introList[value].support)

				if (data.last_format !== undefined)
				{
					$('#dropdown').dropdown('select', data.last_format);
				}
			}

			$('#usage').html(introList[$('#dropdown').dropdown('value')].usage);
		});
	}

	var getStyle = function()
	{
		invoker.invoke("appName", {}, function(data){
			$('body').addClass(data.name);
		});
	}

	var introList = {
		EAN: {
			content: '应用领域：商品、日常物品、图书出版业<br/>' +
					'输入支持：仅数字<br/>' +
					'兼容以下编码：<br/>' +
					'   · EAN-2<br/>' +
					'   · EAN-5<br/>' +
					'   · EAN-8<br/>' +
					'   · EAN-13'
			, test: '9780201379624'
			, usage: '应用领域：商品、日常物品、图书出版业'
			, support: '输入支持：仅数字'
		},
		UPC: {
			content: '应用领域：商品<br/>' +
					'输入支持：仅数字'
			, test: '72527273070'
			, usage: '应用领域：输入支持：商品'
			, support: '输入支持：仅数字'
		},
		CODE128: {
			content: '应用领域：物流业、食品业、医学业 <br/>' +
					'输入支持：支持数字、大小写字母、普通符号以及控制符 <br/>' +
					'兼容以下格式：<br/>' +
					'   · CODE-128 A <br/>' +
					'   · CODE-128 B <br/>' +
					'   · CODE-128 C'
			, test: 'AB+cd-1234%'
			, usage: '应用领域：物流业、食品业、医学业'
			, support: '输入支持：支持数字、大小写字母、普通符号以及控制符'
		},
		CODE39: {
			content: '应用领域：汽车工业、电子工业 <br/>' +
					'输入支持：数字、大写字母、部分符号'
			, test: 'ABC+1234%'
			, usage: '应用领域：汽车工业、电子工业'
			, support: '输入支持：数字、大写字母、部分符号'
		},
		ITF14: {
			content: '应用领域：非零售的商品、纸质包装箱<br/>' +
					'输入支持：仅数字'
			, test: '37596507783323'
			, usage: '应用领域：非零售的商品、纸质包装箱'
			, support: '输入支持：仅数字'
		},
		MSI: {
			content: '应用领域：物流、货物储存领域<br/>' +
					'输入支持：仅数字<br/>' +
					'兼容以下格式<br/>' +
					'   · MSI10  <br/>' +
					'   · MSI11<br/>' +
					'   · MSI1010<br/>' +
					'   · MSI1110<br/>'
			, test: '0123456782'
			, usage: '应用领域：物流、货物储存领域'
			, support: '输入支持：仅数字'
		},
		pharmacode: {
			content: '应用领域：药品包装、联机控制 <br/>' +
					'输入支持：仅数字'
			, test: '12345'
			, usage: '应用领域：药品包装、联机控制'
			, support: '输入支持：仅数字'
		},
		codabar: {
			content: '应用领域：序列号、服务订单、会员卡 <br/>' +
					'输入支持：数字、部分符号'
			, test: '1234567/-+.'
			, usage: '应用领域：序列号、服务订单、会员卡'
			, support: '输入支持：数字、部分符号'
		}
	}

	var $spanDemo2 = null;
	var details = function(item)
	{
		var $content = $('.dropdown-billboard-content');
		$content.html(introList[item.value].content);
		JsBarcode("#demo", introList[item.value].test, {
			format: getFormat(item.value, introList[item.value].test),
			displayValue: true,
			fontSize:14,
			lineColor: "#000",
			height: 60
		});
		$spanDemo2 && $spanDemo2.css('left', $('#demo').position().left).show();
	}

	
	var prepareBillboard = function()
	{
		var $b = $('.dropdown-billboard');
		$('<div/>')
			.addClass('dropdown-billboard-content')
			.css('width', '100%')
			.appendTo($b)

		if(!$spanDemo2)
		{
			$spanDemo2 = $('<span/>')
				.addClass('demo-span-2')
				.html('示例')
				.appendTo($b)
				.hide();
		}

		$('<img/>')
			.attr('id', 'demo')
    		.css('position', 'relative')
			.appendTo(
				$('<div/>')
					.addClass('dropdown-billboard-preview')
					.css('width', '100%')
					.css('height', '110px')
					.css('text-align', 'center')
					.appendTo($b)
			)
	}

	var $spanDemo1 = null;
	var clearDemo = function()
	{
		$('#result')
			.attr('style', '')
			.hide();
		$spanDemo1 && $spanDemo1.hide();
	}

	var viewDemo = function(format)
	{
		if ($('#content').val().length === 0)
		{			
			clearDemo();
			JsBarcode("#result", introList[format].test, {
				format: getFormat(format, introList[format].test),
				displayValue: true,
				fontSize:14,
				lineColor: "#000",
				height: 80
			});

			if (!$spanDemo1)
			{
				$spanDemo1 = $('<span/>')
								.addClass('demo-span-1')
								.html('示例')
								.prependTo($('.preview'))
			}

			$spanDemo1.show();

			return true;
		}
		return false;
	}

	$(function(){
		updateUI(false);
		barcodeDropdown();
		$('#dropdown').dropdown({
			listArray: [
						{value: "CODE128", text: "Code128 (Auto)"},
						{value: "EAN", text: "EAN"},
						{value: "UPC", text: "UPC-A"},
						{value: "CODE39", text: "Code39"},
						{value: "ITF14", text: "ITF14"},
						{value: "MSI", text: "MSI"},
						// {value: "pharmacode", text: "Pharmacode"},
						{value: "codabar", text: "Codabar"}],
			dropdownWidgetSize: [544, 320],
			dropdownListSize: [180, 320],
			dropdownImage: 'css/img/btn_more.png',
			spanClass: 'dropdown-span',
			dropdownClass: 'dropdown',
			dropdownWidgetClass: 'dropdown-widget',
			dropdownListClass: 'dropdown-list',
			dropdownItemClass: 'dropdown-item',
			dropdownBillboard: 'dropdown-billboard',
			clicked: function() {
				details(this.options.listArray[this.index]);
			},
			showed: function() {
				$spanDemo1 && $spanDemo1.hide();
				$('#usage').hide();
			},
			hidden: function(){
				$spanDemo2 && $spanDemo2.hide();
				viewDemo(this.value);
				$('#usage').show();
			},
			changed: function() {
				$('#content').val('');
				adjust();
				viewDemo(this.value);
				$('#content').attr('placeholder', introList[this.value].support)
				$('#usage').html(introList[this.value].usage);
				invoker.invoke('saveSettings', {format: this.index});
			},
			hover: function(item) {
				details(item);
			}
		});

		prepareBillboard();
		adjust();

		$('#content').on('input', function(){
			if ($('#content').val().length === 0)
				$('#log').html('　');
			if (!viewDemo($('#dropdown').dropdown('value')))
				clearPreview();
			viewBarcode();
		});

		$('#insert').click(function(){
			var canvas = document.createElement("canvas");
			var text = $('#content').val();
			var format = getFormat($('#dropdown').dropdown('value'), text);
			JsBarcode(canvas, text, { format: format, height: 100 } );

			var base64 = canvas.toDataURL("image/png");
			invoker.invoke("insert", {base64: base64, format: $('#dropdown').dropdown('value'), text: text}, function(data){
			});
		})

		$('#reject').click(function(){
			invoker.invoke("reject");
		})

		loadData();
		getStyle();
		setTimeout(sizeWatcher, 50);
	})
}(window, jQuery));