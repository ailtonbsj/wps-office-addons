;(function(global){
	var Invoker = function(){
		this.invokeMethod = "jsAsynCall";
		this.invoke = function(methodName, params, callback)
		{
			if (callback !== undefined)
			{
				var callbackName = 'jsCallback' + (new Date() - 0) + Math.ceil(Math.random() * 10000)
				global[callbackName] = function(result) {
					delete global[callbackName];
					callback(result);
				}
			}

			var input = {
				method: methodName,
				callback: callback !== undefined ? callbackName : '',
			};

			var combined = $.extend({}, input, params);

			if (global.cefQuery !== undefined)
			{
				global.cefQuery({
					request: this.invokeMethod + "('" + JSON.stringify(combined) + "')"
				});
			}
			else if (global.qwebkit !== undefined)
			{
				global.qwebkit.qwebkit_invoke(methodName, JSON.stringify(combined));
			}
			else
			{
				callback && callback({});
			}
		}
	}
	global.invoker = new Invoker();
})(window);